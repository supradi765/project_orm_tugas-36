<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function list(){
        $listDataCustomer = Customer::select('nama_customer', 'nomor_telepon', 'alamat_customer')->get();
        return view('customer.list', compact('listDataCustomer'));
    }
    public function formInput(){
        return view('customer.formInput');
    }

    public function simpanData(Request $request){
       
        try {
            $datas = $request->all();
            $customer = new Customer;
            $customer->nama_customer = $datas['nama_customer'];
            $customer->nomor_telepon = $datas['nomor_telepon'];
            $customer->alamat_customer = $datas['alamat_customer'];
            $customer->save();
            return redirect()->route('customer.list')->with('success', _('Berhasil'));
        } catch (\Throwable $th) {
            return redirect()->route('customer.form-input')->with('error', __($th->getMessage()));
        }
    }
}
